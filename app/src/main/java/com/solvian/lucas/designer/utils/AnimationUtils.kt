package com.solvian.lucas.designer.utils

import android.content.Context
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.solvian.lucas.designer.R

class AnimationUtils(context: Context) {

    val startScale : Animation = AnimationUtils.loadAnimation(context, R.anim.anim_start_scale)
    val startScaleFast : Animation = AnimationUtils.loadAnimation(context, R.anim.anim_start_scale_fast)

}