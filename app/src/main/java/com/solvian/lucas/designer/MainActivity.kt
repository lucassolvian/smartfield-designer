package com.solvian.lucas.designer

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.solvian.lucas.designer.databinding.ActivityMainBinding
import com.solvian.lucas.designer.utils.AnimationUtils


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var animation : AnimationUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupView()

    }

    private fun setupView(){
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        startAnimationInSplash()

        binding.loadingMessage.text = "Carregando"
    }

    private fun startAnimationInSplash(){
        animation = AnimationUtils(this)

        binding.avatar.startAnimation(animation.startScaleFast)
        binding.progressBar.startAnimation(animation.startScale)
        binding.loadingMessage.startAnimation(animation.startScale)
    }


}
