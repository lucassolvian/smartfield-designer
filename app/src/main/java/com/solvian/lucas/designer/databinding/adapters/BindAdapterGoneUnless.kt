package com.solvian.lucas.designer.databinding.adapters

import android.view.View
import androidx.databinding.BindingAdapter

    @BindingAdapter("slv:goneUnless")
    fun goneUnless(view: View, visible: Boolean) {
        view.visibility = if (visible) View.VISIBLE else View.GONE
    }


