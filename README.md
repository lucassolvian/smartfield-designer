
[![N|Solid](https://solvian.com/wp-content/uploads/2018/08/logohome.png)](https://solvian.com/)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)]r)

 

  - Kotlin
  - Databinding

# Tratamento de imagens!
| Densidade | Descrição |
| ------ | ------ |
| ldpi | Recursos para telas de baixa densidade (ldpi) (cerca de 120 dpi). |
| mdpi | 	Recursos para telas de média densidade (mdpi) (cerca de 160 dpi). Essa é a densidade básica. |
| hdpi | Recursos para telas de alta densidade (hdpi) (cerca de 240 dpi). |
| xhdpi | Recursos para telas de densidade extra-alta (xhdpi) (cerca de 320 dpi). |
| xxhdpi | Recursos para telas de densidade extra-extra-alta (xxhdpi) (cerca de 480 dpi). |
| xxxhdpi | Recursos para telas de densidade extra-extra-extra-alta (xxxhdpi) (cerca de 640 dpi). |
| nodpi | Recursos para todas as densidades. Esses são recursos independentes de densidade. O sistema não dimensiona recursos marcados com esse qualificador, independentemente da densidade da tela atual. |
|tvdpi | Recursos para telas entre mdpi e hdpi; aproximadamente 213 dpi. Esse não é considerado um grupo de densidade "principal". Ele é destinado principalmente a televisões, e a maioria dos apps provavelmente não precisa dele. Fornecer recursos mdpi e hdpi é o suficiente para a maioria dos apps, e o sistema os dimensionará conforme for apropriado. Se julgar necessário fornecer recursos tvdpi, dimensione-os para um fator de 1,33*mdpi. Por exemplo, uma imagem de 100 x 100 px para telas mdpi precisa ter 133 x 133 px para tvdpi. |
 	
>Para criar drawables de bitmap alternativos para diferentes densidades, siga a taxa de dimensionamento 3:4:6:8:12:16 entre as seis densidades principais. Por exemplo, se tiver um drawable de bitmap de 48 x 48 px para telas de densidade média, os diferentes tamanhos precisarão ser:

    36 x 36 (0,75 x) para densidade baixa (ldpi)
    48 x 48 (configuração básica de 1,0 x) para densidade média (mdpi)
    72 x 72 (1,5 x) para densidade alta (hdpi)
    96 x 96 (2,0 x) para densidade extra-alta (xhdpi)
    144 x 144 (3,0 x) para densidade extra-extra-alta (xxhdpi)
    192 x 192 (4,0 x) para densidade extra-extra-extra-alta (xxxhdpi)

